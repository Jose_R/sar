#Bosch Jorge, Didac
#Prieto Fontcuberta, Jose Ramon


from nltk.corpus import cess_esp


def remove_stopwords(text, language = 'spanish'):
	result = [w for w in text if w.lower() not in stopwords]
	return result
    
quijote_file = open("quijote.txt").read()

print(' '.join(sorted(set(quijote_file))))

quijote_filtered = re.sub('[%s]' % re.escape("'"),'', quijote_filtered)
quijote_filtered = re.sub('[%s]' % re.escape('¡!"(),-.:;¿?]«»)'),'', quijote_file)

print(' '.join(sorted(set(quijote_filtered))))

words = quijote_filtered.split()
print(len(words), " ", len(set(words)))
print(sorted(set(words))[:10], "\n", sorted(set(words))[len(set(words))-10:])

frec = FreqDist(words)
print(frec.most_common(20))


text_no_stopwords = remove_stopwords(words)
text_file = open('quijoteSinStop.txt','w')
for word in text_no_stopwords:
	text_file.write(word+" ")
text_file.close()

print (len(text_no_stopwords), " ", len(set(text_no_stopwords)))
print(sorted(set(text_no_stopwords))[:10], "\n", sorted(set(text_no_stopwords))[len(set(text_no_stopwords))-10:])

frec = FreqDist(text_no_stopwords)
print(frec.most_common(20))

text_file = open('quijoteSinStopStem.txt','w')
stemmer = SnowballStemmer("spanish")
stemmer_Words=[]
for word in text_no_stopwords:
	word=stemmer.stem(word)
	text_file.write(word)
	stemmer_Words.append(word)
text_file.close()


print(len(stemmer_Words), " ", len(set(stemmer_Words)))
print(sorted(set(stemmer_Words))[:10], "\n", sorted(set(stemmer_Words))[len(set(stemmer_Words))-10:])

frec = FreqDist(stemmer_Words)
print(frec.most_common(20))
