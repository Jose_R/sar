
import sys
from nltk.corpus import cess_esp as corpus
from nltk.probability import *
from nltk.corpus import PlaintextCorpusReader

if __name__ == "__main__":


  
	
    print("INTRODUCCION A NLTK")
    print("Obteniendo numero de palabras del corpus")

    corpus_words = corpus.words()
    n_corpus_words = len(corpus_words)


    print("Numero de palabras del corpus: ", n_corpus_words)

    corpus_sents = corpus.sents()
    n_corpus_sents = len(corpus_sents)

    print("Numero de frases del corpus: ", n_corpus_sents)

    first_file = corpus.fileids()[0]
    text = corpus.words(first_file)
    fdist = FreqDist(text)
    most_common = fdist.most_common(20)

    print("20 palabras mas frecuentes: ", most_common )

    vocabulary = [w for w,f in fdist.most_common()]
    print("Vocabulario: \n ", vocabulary)

    palabras_siete = [w for w in vocabulary if len(w) > 7 and fdist[w] > 2]
    print(palabras_siete)

    lista_freq = sorted(list(fdist.values()), reverse = True)
    print(lista_freq)
    print("Freq aparicion de la preposicion a ", fdist['a'])

    n = 0
    for w in lista_freq:
        if w == 1 :
            n += 1
    print("No de palabras que aparecen una sóla vez: ", n)

    palabra_mas_comun = fdist.most_common(1).pop(0)
    print("La palabra mas frecuente es: ", palabra_mas_comun[0])

    directorio = 'corpus'
    miCorpus = PlaintextCorpusReader(directorio, '.*')
    lista = miCorpus.fileids()
 
    for f in lista:  # Calcular el número de palabras, el número de palabras distintas y el número de frases de los tres documentos.
        text = miCorpus.words(f)
        fdist = FreqDist(text)
        n_words = len(miCorpus.words(f))      ##falta descargar cosas de nltk

        vocabulario = len(fdist.keys())
        n_frases = len(miCorpus.sents(f))
        print(f," Numero de palabras totales: ", n_words, " Vocabulario: ",vocabulario,"---- N frases: --- ", n_frases)
        print()
'''
12. Coinciden estos resultados con los de la practica anterior. Justifica la respuesta
No coincidien, ya que en la anterior práctica, para nosotros, las lineas vienen delimitadas por los saltos de linea,
 y para nltk, las linea se separan por puntos.
 Tampoco son las mismas palabras, tanto en el numero total, como en el vocabulario.
 Nosotros solo contemplabamos terminos alfanumericos, mientras que NLTK contempla todo como palabra.

'''



# Ejercicio 2
from nltk.corpus import brown

def contarPalCategories(lista_pal,corpus):
    lista_categorias = brown.categories()
    res = []
    for pal in lista_pal:
        res.append(pal)
        arr = []
        for c in lista_categorias:
            arr.append(c)
            fdist = FreqDist(brown.words(categories=c))
            arr.append(fdist[pal])
        res.append(arr)

    return res

lista_pal = ['what', 'when', 'where', 'who', 'why']
print(contarPalCategories(lista_pal, brown))


