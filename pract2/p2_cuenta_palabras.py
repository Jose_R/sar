#! -*- encoding: utf8 -*-
# inspired by Lluís Ulzurrun and Víctor Grau work
# Alumnos:
# Didac Bosch Jorge
# Jose Ramon Prieto Fontcuberta
# Con ampliacion

from operator import itemgetter
import re
import sys

# Elimina caracteres no alfanumericos
clean_re = re.compile('\W+')


def clean_text(text):
    return clean_re.sub(' ', text)


def sort_dic(d):
    for key, value in sorted(sorted(d.items()), key=itemgetter(1), reverse=True):
        yield key, value


def sort_dic_alphabetically(d):
    for key, value in sorted(d.items()):
        yield key, value


def leer_fichero(name):
    f = open(name, 'r')
    lines = []
    for line in f:
        lines.append(clean_text(line))
    f.close()

    return lines


def load_stopwords(filename):
    f = open(filename, 'r')
    lines = []
    for line in f:
        pal = clean_text(line).strip()
        lines.append(pal)
    f.close()
    return lines


def remove_stopWords(palabras, stopwords):
    lista_pal = [palabra for palabra in palabras if palabra.lower() not in stopwords]
    return lista_pal


# Ampliacion
def contar_pares(voc_pairs, voc_letras_pairs, vector_palabras=[]):
    vector_palabras.append("$")
    vector_palabras.insert(0, "$")
    for x in range(len(vector_palabras) - 1):
        p1 = vector_palabras[x]
        p2 = vector_palabras[x + 1]
        pair = p1 + " " + p2
        # --- pair
        p = voc_pairs.get(pair)
        if p is None:
            voc_pairs[pair] = 1
        else:
            voc_pairs[pair] = p + 1
        # fin pairs

        # ---- letras
        for x in range(len(p1) - 1):
            l1 = p1[x]
            l2 = p1[x + 1]
            letras_pair = l1 + l2
            l = voc_letras_pairs.get(letras_pair)
            if l is None:
                voc_letras_pairs[letras_pair] = 1
            else:
                voc_letras_pairs[letras_pair] = l + 1
                # fin letras
    return voc_pairs, voc_letras_pairs


def cuenta_palabras(lineas, to_lower=False, remove_stopwords=False):
    n = 0
    n_words_wihout = 0
    voc = dict()
    voc_letras = dict()
    voc_pairs = dict()
    voc_letras_pairs = dict()
    n_letras = 0
    if remove_stopwords:
        stopwords = load_stopwords("stopwords_en.txt")

    for l in lineas:
        l = l.strip()
        if to_lower:
            l2 = l.lower()
            l = l.lower()
        else:
            l2 = l
        s = l2.split(' ')
        sPairs = l.split(' ')  # con mayus
        voc_pairs, voc_letras_pairs = contar_pares(voc_pairs, voc_letras_pairs, sPairs)
        n += len(s)
        if remove_stopwords:
            s = remove_stopWords(s, stopwords)
        n_words_wihout += len(s)
        for palabra in s:
            n_letras += len(palabra)
            voc_letras = vocabulario_simbolos(palabra, voc_letras=voc_letras)
            p = voc.get(palabra)
            if p is None:
                voc[palabra] = 1
            else:
                voc[palabra] = p + 1

    return n_words_wihout, n, voc, n_letras, voc_letras, voc_pairs, voc_letras_pairs


def vocabulario_simbolos(palabra, voc_letras=dict()):
    for letra in palabra:
        l = voc_letras.get(letra)
        if l is None:
            voc_letras[letra] = 1
        else:
            voc_letras[letra] = l + 1
    return voc_letras


def text_statistics(filename, to_lower=False, remove_stopwords=False):
    lineas = leer_fichero(filename)
    n_lines = len(lineas)
    n_words_wihout, n_words, voc, n_symb, voc_symb, voc_pairs, voc_letras_pairs = cuenta_palabras(lineas,
                                                                                                  to_lower=to_lower,
                                                                                                  remove_stopwords=remove_stopwords)
    print("Lines: %d" % n_lines)

    print("Number words (with stopwords): %d" % n_words)

    if remove_stopwords:
        print("Number words (without stopwords): %d" % n_words_wihout)

    print("Vocabulary size: %d" % len(voc))
    print("Number of symbols: %d" % n_symb)
    print("Number of different symbols: %d " % len(voc_symb))
    print("-------------")
    print_voc(voc, "Words", "alphabetical")
    print("-------------")
    print_voc(voc, "Words", "frequency")
    print("-------------")
    print("-------------")
    print_voc(voc_symb, "Symbols", "alphabetical")
    print("-------------")
    print_voc(voc_symb, "Symbols", "frequency")
    print("-------------")

    print("-------------")
    print_voc(voc_pairs, "Word Pairs", "alphabetical")
    print("-------------")
    print_voc(voc_pairs, "Word Pairs", "frequency")
    print("-------------")
    print("-------------")
    print_voc(voc_letras_pairs, "Symbols Pairs", "alphabetical")
    print("-------------")
    print_voc(voc_letras_pairs, "Symbols Pairs", "frequency")
    print("-------------")


def print_voc(voc, name, orden):
    print("%s (%s):" % (name, orden))
    if orden == "frequency":
        voc = sort_dic(voc)
        for key, value in voc:
            print("%s %d" % (key, value))
    else:
        voc = sort_dic_alphabetically(voc)
        for key, value in voc:
            print("%s %d" % (key, value))


def syntax():
    print("\n%s filename.txt [to_lower?[remove_stopwords?]\n" % sys.argv[0])
    sys.exit()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        syntax()
    name = sys.argv[1]
    lower = False
    stop = False
    if len(sys.argv) > 2:
        lower = (sys.argv[2] in ('1', 'True', 'yes'))
        if len(sys.argv) > 3:
            stop = (sys.argv[3] in ('1', 'True', 'yes'))

    text_statistics(name, to_lower=lower, remove_stopwords=stop)