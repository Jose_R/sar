#!/usr/bin/env python
## Miembros:
#Didac Bosch Jorge
# Jose R. Prieto Fontcuberta
from whoosh.index import open_dir
from whoosh.qparser import QueryParser
from whoosh.query import *
import zipfile,os,shutil
from xml.dom.minidom import parseString

temp = "tmp"
ix = open_dir("index_dir")
fichero = 'mini_enero.zip'
def validarXML(xml, file=None):
    if file:
        fileXML = open(xml, "r",encoding="utf-8")
        xml = fileXML.read()
    xml = "<xml>" + xml + "</xml>"
    return xml

def devolverNoticia(path,position):
    print("buscar en ",path, " posicion ", position)
    dom = parseString(validarXML(path, file=True))
    docs = dom.getElementsByTagName('DOC')
    res =  docs[position].getElementsByTagName('TEXT')[0].firstChild.nodeValue
    return res
    #i = 1  # campo para ids, en cada documento empezara por 0
    #for d in docs:
     #   fecha = d.getElementsByTagName('DATE')[0].firstChild.nodeValue
      #  titulo = d.getElementsByTagName('TITLE')[0].firstChild.nodeValue
       # texto = d.getElementsByTagName('TEXT')[0].firstChild.nodeValue
        #i += 1

with ix.searcher() as searcher:
    '''Querys del ejercicio: '''


    def unaQuery(q, q2=None, qand=False):
        with ix.searcher() as searcher:

            if not qand:
                print("Query: ", q)
                query = QueryParser("content", ix.schema).parse(q)
            else:
                print("Query: ", q, " and ", q2)
                q = And([Term("content", q), Term("content", q2)])
                query = QueryParser("content", ix.schema).parse(q)
            results = searcher.search(query, limit=None)

            print("Numero de resultados: " , len(results))

            #print("Times: ", len(results))


    # shutil.rmtree(temp, ignore_errors=True) #borrar tmp


    # Busca los documentos que contengan el texto valencia

    unaQuery("Valencia")
    # Busca valencia con la restricción de que no aparezca el término Salenko (13 documentos).
    unaQuery("Salenko")
    # Busca documentos con el texto futbol (31 documentos).
    unaQuery("futbol")
    # Busca los términos Los Angeles y Aeroflot (3 documentos).
    unaQuery("Los Angeles AND Aeroflot")
    text = input("Dime:")
    while len(text) > 0:
        query = QueryParser("content", ix.schema).parse(text)
        results = searcher.search(query, limit=None)
        long = len(results)

        for r in results:
            print("Fecha: " , r['date'])
            print("Noticia numero ",r["position"], " En el documento ", r['path'])
            print("ID global: ", r['id'])
            print("Titulo: ", r['title'])
            if long < 4:
                with zipfile.ZipFile(fichero) as zip:
                    if not os.path.exists(temp):
                        os.mkdir(temp)
                    zip.extractall(temp)

                text = devolverNoticia(r['path'],int(r["position"]))
                print("Texto: ",text)
                shutil.rmtree(temp, ignore_errors=True)  # borrar tmp
            #print (r)
            print()

        print("Times: " , len(results))
#        print (dir(results))
#        print (results.docs)
        text = input("Dime más:")


