#!/usr/bin/env python

import os,zipfile,re,shutil
from whoosh import index
from whoosh.fields import Schema, ID, TEXT
# match = re.sub('\w+\/', '', lf)
from whoosh.analysis import *
from whoosh.qparser import QueryParser
from whoosh.query import *
## Miembros:
#Didac Bosch Jorge
# Jose R. Prieto Fontcuberta

temp = "tmp"
fichero_noticias = "mini_enero.zip"

#  Utiliza varios analizadores distintos y comprueba como afecta eso al tamao del ndice. Justifica
# por qu ocurre esto.
#Analizadores:

#Tamaño del indice
# sin analizador 39MB

# Primera pregunta:

"""
Utiliza varios analizadores distintos y comprueba como afecta eso al tamaño del índice. Justifica
por qué ocurre esto.
# con stemminganalyzer
# 88.196
#  my_analyzer =  StemmingAnalyzer() ~ NgramFilter(minsize=2, maxsize=4)
# 334


El tamanyo va en aumento debido a que crece el diccionario
"""

""" Primer ejercicio:"""
"""
my_analyzer =  StemmingAnalyzer() | NgramFilter(minsize=2, maxsize=4)
schema = Schema(title=TEXT(stored=True, analyzer=my_analyzer), path=ID(stored=True), content=TEXT)
idir = "index_dir"

if not os.path.exists(idir):
    os.mkdir(idir)

ix = index.create_in(idir, schema)
writer = ix.writer()

with zipfile.ZipFile('enero.zip') as zip:
    if not os.path.exists(temp):
        os.mkdir(temp)
    zip.extractall(temp)

#dirpath:  tmp\enero  dirnames:  []  filenames: ['19940101.sgml'
for (dirpath, dirnames, filenames) in os.walk(temp):
    #print("dirpath: " , dirpath , " dirnames: " , dirnames , " filenames:" ,filenames)
    for filename in filenames:
        fichero = dirpath + "\\" + filename
        print("fichero: " , fichero)

        f = open(fichero)
        writer.add_document(title=filename, path=fichero, content=f.read())
writer.commit()
"""
#Segundo ejercicio

my_analyzer =  StemmingAnalyzer()
schema = Schema(title=TEXT(stored=True), path=ID(stored=True), content=TEXT(stored=False,analyzer=my_analyzer), date=TEXT(stored=True), id=TEXT(stored=True), position=TEXT(stored=True))
idir = "index_dir"

def validarXML(xml, file=None):
    if file:
        fileXML = open(xml, "r",encoding="utf-8")
        xml = fileXML.read()
    xml = "<xml>" + xml + "</xml>"
    return xml

from xml.dom.minidom import parseString
#Descomprimimos zip

with zipfile.ZipFile(fichero_noticias) as zip:
    if not os.path.exists(temp):
        os.mkdir(temp)
    zip.extractall(temp)

if not os.path.exists(idir):
    os.mkdir(idir)

ix = index.create_in(idir, schema)
writer = ix.writer()

for (dirpath, dirnames, filenames) in os.walk(temp):
    print("dirpath: " , dirpath , " dirnames: " , dirnames , " filenames:" ,filenames)
    for filename in filenames:
        fichero = dirpath + "/" + filename
        print("fichero: " , fichero)
        #por cada sgml y/o archivo
        #lo validamos como xml. Un XML empieza por xml y se contiende to_do dentro de esta etiqueta
        dom = parseString(validarXML(fichero, file=True))
        #En dom tenemos to do ordenado ya
        docs=dom.getElementsByTagName('DOC')
        i = 0 #campo para ids, en cada documento empezara por 0
        for d in docs:
            fecha = d.getElementsByTagName('DATE')[0].firstChild.nodeValue
            titulo = d.getElementsByTagName('TITLE')[0].firstChild.nodeValue
            texto = d.getElementsByTagName('TEXT')[0].firstChild.nodeValue
            id = fichero + "_"+ str(i)
            print("id: ",id)
            writer.add_document(date=fecha, id=id, title=titulo, path=fichero,content=texto, position=str(i))
            i+=1

            #input("Press: ")


writer.commit()
#borramos lo descromprimido
shutil.rmtree(temp, ignore_errors=True) #borrar tmp

