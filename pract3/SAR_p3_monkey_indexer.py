# Alumnos:
# Carlos Longares Alcala
# Jose Ramon Prieto Fontcuberta
import re,sys,pickle
DEBUG = False
clean_re = re.compile('\W+')
tokens = ['!','?',':',';','.',"\n\n"]
#tokens = ['.',"\n"]

def clean_text(text):
    return clean_re.sub(' ', text)


def sort_dic(d):
    for key, value in sorted(sorted(d.items()), key=itemgetter(1), reverse=True):
        yield key, value


def sort_dic_alphabetically(d):
    list = []
    for key, value in sorted(d.items()):
        list.append((key,value))
    return list

def leer_fichero(name):
    f = open(name, 'r', encoding='utf-8')
    content = f.read()
    f.close()
    #content = content.replace('.', '\n').split("\n")
    for token in tokens:
        content = content.replace(token,"\n\n")
    content = content.split("\n\n")
    lines = []
    for line in content:
        line = line.lower()
        line = clean_text(line).strip()
        lines.append(line)

    return lines

def vocabulario_simbolos(palabra, voc_letras=dict()):
    for letra in palabra:
        l = voc_letras.get(letra)
        if l is None:
            voc_letras[letra] = 1
        else:
            voc_letras[letra] = l + 1
    return voc_letras

def cuenta_palabras(lineas, to_lower=False):
    voc = dict()
    for l in lineas:
        l = l.strip()
        if to_lower:
            l = l.lower()
        s = l.split(' ')
        if (s[-1] != "$"):
            s.append("$")
        if (s[0] != "$"):
            s.insert(0, "$")
        for palabra in s:
            p = voc.get(palabra)
            if p is None:
                voc[palabra] = 1
            else:
                voc[palabra] = p + 1

    return voc

# Se le pasa una linea y devuelve los pares de palabras y letras, actualizando las listas ya dadas
def contar_pares(voc_pairs=dict(), voc_letras_pairs=dict(), vector_palabras=[]):
    if(type(vector_palabras)==str):
        vector_palabras = vector_palabras.split(" ")
    vector_palabras.append("$")

    vector_palabras.insert(0, "$")
    for x in range(len(vector_palabras) - 1):
        p1 = vector_palabras[x].lower()
        p2 = vector_palabras[x + 1]
        pair = p1 + " " + p2.lower()
        # --- pair
        p = voc_pairs.get(pair)
        if p is None:
            voc_pairs[pair] = 1
        else:
            voc_pairs[pair] = p + 1
        # fin pairs

        # ---- letras
        for x in range(len(p1) - 1):
            l1 = p1[x]
            l2 = p1[x + 1]
            letras_pair = l1 + l2
            l = voc_letras_pairs.get(letras_pair)
            if l is None:
                voc_letras_pairs[letras_pair] = 1
            else:
                voc_letras_pairs[letras_pair] = l + 1
                # fin letras
    return voc_pairs, voc_letras_pairs


"""
pairs : {'and Bacon': 2, 'egg on': 1, 'beans Spam': 1, 'top and': 1, 'Egg sausage': 1, 'crevettes with': 1, 'baked beans': 1, 'Bacon sausage': 2, 'and a': 1, 'Lobster Thermidor': 1, 'with truffle': 1, 'Spam Egg': 3, 'sausage and': 3, 'fried egg': 1, 'with a': 1, 'Egg and': 3, 'Spam baked': 1, 'pate brandy': 1, 'truffle pate': 1, 'Thermidor aux': 1, 'Spam $': 9, 'a fried': 1, 'sauce garnished': 1, 'brandy and': 1, 'a Mornay': 1, 'on top': 1, 'garnished with': 1, 'Spam Bacon': 2, 'and Spam': 9, '$ Egg': 5, 'Egg Spam': 1, 'Sausage and': 1, 'Spam Spam': 11, '$ Spam': 5, 'Mornay sauce': 1, 'Bacon $': 2, '$ Lobster': 1, 'Egg Bacon': 2, 'aux crevettes': 1, 'Egg Sausage': 1, 'Bacon and': 2, 'Spam and': 1}
voc_palabras: [('$', 22), ('a', 2), ('and', 12), ('aux', 1), ('bacon', 6), ('baked', 1), ('beans', 1), ('brandy', 1), ('crevettes', 1), ('egg', 9), ('fried', 1), ('garnished', 1), ('lobster', 1), ('mornay', 1), ('on', 1), ('pate', 1), ('sauce', 1), ('sausage', 4), ('spam', 2"""
def compresor(voc_palabras, pairs):
    #print("Comprimiendo..")
    list = []
    if DEBUG:
        print("Longitud vocabulario: %d" % len(voc_palabras))
        print("Numero de pares: %d"%len(pairs))
    for tuple_word in voc_palabras:
        word = tuple_word[0]
        n = tuple_word[1]
        sucesivas = buscar_en_pairs(pairs,word)
        list.append([word,n,sucesivas])
    #quitamos la mitad de $
    l = list[0]
    l[1] = int(l[1]/2)
    list[0] = l
    return list

#busca en un dict en el que el indice empiece por la palabra dada
#devuelve una lista
def buscar_en_pairs(pairs, palabra):
    lista = []
    for key, value in pairs.items():
        try:
            pal1, pal2 = key.split()
        except ValueError:
            pass
        if palabra == pal1.lower() and pal1 is not None and pal1 != "" and pal2 is not None and pal2 != "":
            lista.append([value,pal2.lower()])
    return sorted(lista,reverse=True)

def indexer(filename):
    list = dict()
    pares = dict()
    voc_palabras = dict()
    file = leer_fichero(filename)
    # Pares:
    for line in file:
        pares,_ = contar_pares(voc_pairs=pares, vector_palabras=line)
    #Fin pares
    #Contar palabras:
    voc_palabras = sort_dic_alphabetically(cuenta_palabras(file,to_lower=True))
    list = compresor(voc_palabras,pares)

    return list
def syntax():
    print("\n%s filename.txt filenameToave.txt\n" % sys.argv[0])
    sys.exit()

def save_object(object, file_name):
    with open(file_name, 'wb') as fh:
        pickle.dump(object, fh)

def main(filename, filenameToSave):
    list = indexer(filename=filename)
    save_object(list, filenameToSave)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        syntax()

    filename = sys.argv[1]
    filenameToSave = sys.argv[2]

    #filename = "spam.txt"
    #filename2 = "tirantloblanc.txt"
    #filenameToSave = "resultado_" + filename
    list = indexer(filename=filename)
    if DEBUG:
        for x in list:
            print (x)

    save_object(list,filenameToSave)

