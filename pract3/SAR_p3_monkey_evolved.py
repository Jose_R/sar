# Alumnos:
# Carlos Longares Alcala
# Jose Ramon Prieto Fontcuberta
import sys,pickle,random

def load_object(file_name):
    with open(file_name, 'rb') as fh:
        obj = pickle.load(fh)
    #print (obj)
    return obj

def convert_list_to_dict(list):
    d = dict()
    for i in range(len(list)):
        line_word = list[i]
        d[line_word[0]] = [line_word[1],line_word[2]]
    return d

def generar_palabra(list):

    frase = "$"

    dictionary_list = convert_list_to_dict(list)
    cont = 1
    word = "$"
    while True:

        words = dictionary_list.get(word) # = [27, [(11, 'spam'), (9, '$'), (3, 'egg'), (2, 'bacon'), (1, 'baked'), (1, 'and')]]
        words = words[1]  # = [(11, 'spam'), (9, '$'), (3, 'egg'), (2, 'bacon'), (1, 'baked'), (1, 'and')]]
        ws = []
        prob = 0
        for x in words:
            prob += x[0]

        #aleatoriedad
        dado = random.randint(0,prob)
        for x in words:
            if(x[0] >= dado ):
                word = x[1]
                break;
            dado -= x[0]
        frase += word + " "
        cont += 1
        if (word == "$" or cont >= 25):
            break;
    return frase

if __name__ == "__main__":

    filename = sys.argv[1]
    #filename2 = "resultado_tirantloblanc.txt"
    #filename = "resultado_spam.txt"
    #indexer.main(filename, "resultado_tirantloblanc.txt")
    list = load_object(filename)
    frase = generar_palabra(list)
    print(frase)
    print("Len: %d" % len(frase.split()))
