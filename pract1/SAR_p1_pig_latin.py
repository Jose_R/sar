#!/usr/bin/env python
#! -*- encoding: utf8 -*-
# 1.1.- Pig Latin
"""
1. -Pig Latin

Nombre Alumno: Jose Ramon Prieto Fontcuberta
Nombre Alumno: Dídac Bosch Jorge
Se ha realizado la ampliacion de la practica usando ficheros
"""
import sys
import argparse


FLAGS = None

"""Si una palabra no comienza con una letra se deja igual.
Para las palabras que comienzan con consonantes, se mueven todas las consonantes antes de la
primera vocal al final y se agrega la sílaba “ay”.
Para palabras que comienzan con vocal (incluyendo la y), simplemente se agrega “yay” al final
de la palabra.
Se deben respetar las mayúsculas en dos casos:
• si una palabra empieza con mayúscula su traducción también debe empezar con mayúscula.
• si una palabra está toda en mayúsculas su traducción debe estar toda en mayúsculas.

"""
def piglatin_word(word):
    """
    Esta funcion recibe una palabra en ingles y la traduce a Pig Latin
    :param word: la palabra que se debe pasar a Pig Latin
    :return: la palabra traducida
    """
    if len(word) <= 0:
        return word
    if not word[0].isalpha():
        return word
    vocal = isVocal(word[0].lower()) # comienza por vocal = True
    if vocal:
        word += "yay"
    else:
        upper = False
        allUpper = False
        if word.isupper():
            allUpper = True
        elif word[0].isupper():
            upper = True
            word = word.lower()
        signo = None
        if esSigno(word[-1]):
            signo = word[-1]
            word = word[:-1]
        for letter in word:
            if not isVocal(letter):
                word += word[0]
                word = word[1:]
            else: break;
        word += "ay"
        if allUpper:
            word = word.upper()
        elif upper and word[0].islower():
            cap = word[0].upper()
            word = cap + word[1:]
        if signo is not None:
            word += signo
    return word

def isVocal(v):
    return v in ('a', 'e', 'i', 'o', 'u','y')

def esSigno(v):
    return v in (',', ';', '.', '?', '!');

def piglatin_sentence(sentence):
    """
    Esta funcion recibe una frase en ingles i la traduce a Pig Latin
    :param sentence: la frase que se debe pasar a Pig Latin
    :return: la frase traducida
    """

    sentences = sentence.split(" ")
    sentence = []
    for s in sentences:
        sentence.append(piglatin_word(s))
    return ' '.join(sentence)

""" Ampliacion"""
def leer_fichero(name):
	f = open(name, 'r')
	filename = name[:-4] + "_piglatin.txt"
	fWrite = open(filename, 'w+')
	for line in f:
		line = line[:-1]
		fWrite.write(piglatin_sentence(line)+"\n")
	f.close()
	fWrite.close()
	

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f',
                        type=str,
                        default='',
                        help='Fichero a leer')
    FLAGS, unparsed = parser.parse_known_args()
    if len(sys.argv) > 1:
        if FLAGS.f != '':
            leer_fichero(FLAGS.f)
        else:
            print (piglatin_sentence(sys.argv[1]))
    else:
        while True:
            x = input("ENGLISH: ")
            if x == '': break;
            print("PIG LATIN: " + piglatin_sentence(x))
