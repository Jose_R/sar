import os,re,shutil,sys,pickle
from xml.dom.minidom import parseString
import math
from copy import deepcopy

_DEBUG = False


class Noticia:
    global texto, titulo, posicion, documento, fecha, posting
    
    def __init__(self, texto, titulo, posicion, documento, fecha,posting = []):
        super().__init__()
        self.texto = texto
        self.titulo = titulo
        self.posicion = posicion
        self.documento = documento
        self.fecha = fecha
        self.posting = posting

    def __str__(self, *args, **kwargs):
        return self.titulo[:30]


def syntax():
    print("\n%s [fichero con indices]\n" % sys.argv[0])
    sys.exit()


"""
Devuelve primero las postings  headline, text, category y date en un dict
ultimo la lista de dias y numero de noticias en ese dia
postings[termino]
#id_noticia = tupla (fichero, noticiaNumeroX)
Cafe [[id_noticia, [0 48]], [noticia_2, [50]], [noticia_58, [0,3748,5145684]], [noticia_X, listaConLasAparacionesEnLanoticia]]

noticias: La lista_dias_noticias
"""
def recuperar_postings(path):
    file = open(path, 'rb')
    recuperar = pickle.load(file)
    postings = dict()
    postings["text"] = recuperar[0]
    postings["category"] = recuperar[1]
    postings["headline"] = recuperar[2]
    postings["date"] = recuperar[3]
    return postings, recuperar[4]

def validarXML(xml, file=None):
    if file:
        fileXML = open(xml, "r",encoding="utf-8")
        xml = fileXML.read()
    xml = "<xml>" + xml + "</xml>"
    return xml

"""
Devolvera un array de objetos noticia
Se espera un array de noticias en forma de:
(n[0], n[1])
n[0] = mini_enero/199xxx.sgml
n[1] = numero de la noticia, empezando por 0

Return [Noticias]"""
def devolver_noticias(noticias):
    #print(noticias)
    res = []
    # no tenemos el nombre de la carpeta, por lo que de momento lo sacamos del mismo nombre
    pathZip = noticias[0][0][0].split("/")[0]
    #print(pathZip)
    for n in noticias:
        if _DEBUG:
            print(" -------------- %s " % n)
        noticia = n[0]
        posting = n[1]
        path = noticia[0]
        position = noticia[1]
        dom = parseString(validarXML(path, file=True))
        docs = dom.getElementsByTagName('DOC')
        date = docs[position].getElementsByTagName('DATE')[0].firstChild.nodeValue
        fecha = date[0:4] + "-" + date[4:6] + "-" + date[6:8]
        noticia = Noticia(
            texto=docs[position].getElementsByTagName('TEXT')[0].firstChild.nodeValue,
            fecha=fecha,
            titulo=docs[position].getElementsByTagName('TITLE')[0].firstChild.nodeValue,
            posicion=position,
            documento=path,
            posting=posting)
        res.append(noticia)
        #print(noticia)
        #print(noticia.fecha)
    return res
        

"""Busca la consulta, un solo termino y consecutivos como si fuese uno solo
ej buscar hola: [('mini_enero/19940106.sgml', 241): [442], ('mini_enero/19940105.sgml', 151): [9]]"""
def busqueda_noticias(termino,postings=dict()):
    #print(termino)
    consecutivos = False
    termino = termino.lower()
    if len(termino.split(" ")) > 1:
        consecutivos = True
    if not consecutivos:
        arrs = postings.get(termino, [])
        if arrs == []: return [];
        if _DEBUG:
            print("devolver noticias con termino %s : %s " % (termino,arrs))
        #print("type %s  " % type(arrs[0]))
        #print("type %s  " % arrs[0])
        return arrs
    else:
        noticias = []
        terminos = termino.split(" ")
        primera = True
        arrs = []
        for t in terminos:
            if primera: # Primera consulta de dos pares de terminos
                arrs.append(postings.get(t,[]))
                if len(arrs) == 2:
                    noticias = buscar_consecutivo(arrs[0], arrs[1])
                    primera = False
            else:
                noticias = buscar_consecutivo(noticias,postings.get(t,dict()))
        return noticias

"""listas del estilo devuelto por las postings
[('mini_enero/19940106.sgml', 241): [442], ('mini_enero/19940105.sgml', 151): [9]]
mira si hay algun index inmediatamente despues en la otra lista
50 51
20 21

Devuelve un array de noticias donde se encuentra el segundo termino
[ ('mini_enero/19940106.sgml', 241) : [pos1, pos34] ),
(   ('mini_enero/19940106.sgml', 241) : [pos1, pos34] )]"""
def buscar_consecutivo(lista1=[], lista2=[]):
    res = []
    k = 0
    #lista2 = []
    while k < len(lista1): # Por cada noticia
        l1 = lista1[k]
        #Buscamos la noticia en lista2
        k += 1
        l2 = None
        j = 0
        while j < len(lista2) and len(lista2) != 0 :
            l2 = lista2[j]
            j += 1
            comp = compareTo(l1[0],l2[0])
            if comp != 0:
                l2 = None

            else:
                break

        if l2 is None: continue; #Si la noticia no existe, pasamos a la siguiente

        #Ejemplo
        # L1 [('mini_enero/19940105.sgml', 2), [28, 42]] y l2 [('mini_enero/19940105.sgml', 2), [29]]
        noticia = l1[0]
        # l1 y l2 pasan a ser las posting solo
        l1 = l1[1]
        l2 = l2[1]
        #Buscamos la posicion +1
        i = 0; j=0;

        while i < len(l1):
            resNoticia = []
            posL1 = l1[i]
            i += 1
            while j < len(l2):
                posL2 = l2[j]
                if posL1+1 == posL2:
                    resNoticia.append(posL2)
                elif posL1 < posL2: break;
                j += 1
            if len(resNoticia) > 0:
                res.append([noticia, resNoticia])
    return res

"""noticia = Noticia
Si hay entre 3 y 5 noticias relevantes. Se mostrará el titular de cada noticia y un
snippet del cuerpo de la noticia que contenga los términos buscados.
Un snippet de un termino es una subcadena del documento que contiene el termino y
un contexto por la izquierda y derecha. Prueba diferentes tamaños de contexto.
"""
#Vamos a devolver tantos snippets como terminos tenga la consulta, 
def sacar_snippet(noticia, l_consulta):


    texto_min = noticia.texto.lower()
    output = ""
    tipo_string = type('')
    operacion = "AND"
    offset = 35


    for term in l_consulta:
        if type(term) == tipo_string:
            operacion = term

        #es un termino
        else:
            if(term[0] == "text" and operacion != "NOT"):
            # es un termino en el texto
                pos = texto_min.find(term[1])
                if pos != -1:
                    min = pos - offset
                    max = pos + offset

                    while(noticia.texto[min:min+1] != " " and min > 0):
                        min = min - 1

                    while (noticia.texto[max:max + 1] != " " and max < len(texto_min)):
                        max = max - 1

                    if min < 0:
                        min = 0
                    if max > len(texto_min):
                        max = len(texto_min)

                    output = output + "..."+noticia.texto[min:max].replace('\n', '')+"...\n"




    return output


"""Compara dos  noticias con el formato ('carpeta/AAAAMMDD.ext', numNoticia) y devuelve > 0 si el primero es mayor, <0 si el segundo o 0 si son iguales
ejemplo: ('mini_enero/19940104.sgml', 68), ('mini_enero/19940104.sgml', 85)  la segunda es mayor"""
def compareTo(noticia1, noticia2):
    #print("noticia1 %s noticia2 %s " % (noticia1, noticia2))
    if(noticia1 < noticia2):
        return -1
    elif(noticia1 > noticia2):
        return 1
    return 0

#estas operaciones se deben implementar con los algoritmos de posting lists vistos en teoria

#noticias1 y noticias2 son listas de noticias
def OR(noticias1, noticias2):
    res = [];
    n1 = 0;
    n2 = 0
    while (n1 < len(noticias1) and n2 < len(noticias2)):
        not1 = noticias1[n1][0]
        not2 = noticias2[n2][0]
        # print("n1 %d long noticias1 %d y n2 %d long noticias2 %d" %(n1,len(noticias1),n2,len(noticias2)))
        comp = compareTo(not1, not2)
        if comp == 0:
            res.append(noticias1[n1])
            n1 += 1
            n2 += 1
        else:
            if comp < 0:
                res.append(noticias1[n1])
                n1 += 1
            else:
                res.append(noticias2[n2])
                n2 += 1
    while n1 < len(noticias1):

        res.append(noticias1[n1])
        n1 += 1
    while n2 < len(noticias2):

        res.append(noticias2[n2])
        n2 += 1
    return res

def AND(noticias1, noticias2):
    res = []; n1 = 0; n2 = 0

    while(n1 < len(noticias1) and n2 < len(noticias2)):
        not1 = noticias1[n1][0]
        not2 = noticias2[n2][0]
        #print("n1 %d long noticias1 %d y n2 %d long noticias2 %d" %(n1,len(noticias1),n2,len(noticias2)))
        #print("Compareto entre noticias1 %s y noticias2 %s " %(not1,not2))
        comp = compareTo(not1,not2)
        if comp == 0:
            res.append(noticias1[n1])
            n1 += 1
            n2 += 1
        else:
            if comp < 0: n1 += 1
            else: n2 += 1
    return res

"""lista de entrada: [['mini_enero/19940101.sgml', 213], ['mini_enero/19940102.sgml', 251], ['mini_enero/19940103.sgml', 457], ['mini_enero/19940104.sgml', 496], ['mini_enero/19940105.sgml', 594], ['mini_enero/19940106.sgml', 462], ['mini_enero/19940107.sgml', 468], ['mini_enero/19940108.sgml', 310], ['mini_enero/19940109.sgml', 303]]

salida: [(mini_enero/19940101.sgml, 0), (mini_enero/19940101.sgml, 1) ... hasta el fin]
"""
def devolver_noticias_dia(lista_dias_noticias):
    universo = []
    for tdia in lista_dias_noticias:
        # tdia = ['mini_enero/19940101.sgml', 213]
        dia = tdia[0]

        for noticia in range(tdia[1]):
            universo.append((dia,noticia))
    return universo

def NOT(noticias1,lista_dias_noticias):
    #print("NOT!!")
    universo_noticias = devolver_noticias_dia(lista_dias_noticias)
    print("Universo: ", len(universo_noticias))

    #print("Universo de noticias: %s " % universo_noticias)
    #print("Len %d " % len(universo_noticias))
    #print("universo_noticias %s \n tipo %s long %d" % (universo_noticias, type(universo_noticias), len(universo_noticias)))
    res = [];
    n1 = 0;
    n = 0
    notN = None
    #print("n1 %d n %d len(noticias1) %d len(univ) %d" % (n1,n,len(noticias1), len(universo_noticias)))
    while (n1 < len(noticias1) and n < len(universo_noticias)):
        not1 = noticias1[n1][0]
        notN = universo_noticias[n]
        comp = compareTo(not1, notN)
        if comp == 0:
            n1 += 1
            n += 1
        else:
            if comp < 0:
                #print(not1)
                #res.append(noticias1[n1])
                n1 += 1
            else:
                #print(notN)
                res.append((universo_noticias[n],[])) # Lista vacia -> No hay apariciones de ese termino en esa noticia
                n += 1
    while n < len(universo_noticias):

        #print(notN)
        res.append((universo_noticias[n],[])) # Lista vacia -> No hay apariciones de ese termino en esa noticia
        n += 1
    return res


def NAND(noticias1, noticias2): #noticias1 - noticias2
    res = [];
    n1 = 0;
    n2 = 0
    not1 = None
    while (n1 < len(noticias1) and n2 < len(noticias2)):
        not1 = noticias1[n1][0]
        not2 = noticias2[n2][0]
        # print("n1 %d long noticias1 %d y n2 %d long noticias2 %d" %(n1,len(noticias1),n2,len(noticias2)))
        comp = compareTo(not1, not2)
        if comp == 0:
            n1 += 1
            n2 += 1
        else:
            if comp < 0:
                res.append(noticias1[n1])
                n1 += 1
            else:
                n2 += 1
    while n1 < len(noticias1):
        res.append(noticias1[n1])
        n1 += 1
    return res


def switch(operacion, noticias1, noticias2=None, lista_dias_noticias=None):
    if _DEBUG: print("switch operacion %s \n noticias1  %s \n noticias2  %s " % (operacion,noticias1, noticias2))
    if operacion == "AND" or operacion == None:
        res = AND(noticias1, noticias2)
        #print("Operacion AND resultado : \n %s " % res)
        return res
    if operacion == "OR":
        return OR(noticias1, noticias2)
    if operacion == "ORNOT" or operacion == "NOTOR":
        resNOT = NOT(noticias2, lista_dias_noticias)
        #print("Len noticias1 %d len resNOT %d " % (len(noticias1), len(resNOT)) )
        #print("noticias1 %s resNOT %s " % (noticias1, resNOT) )
        return OR(noticias1,resNOT)
    if operacion == "ANDNOT" or operacion == "NOTAND":
        return NAND(noticias1, noticias2)
    if operacion == "NOT":
        resNOT = NOT(noticias1, lista_dias_noticias)
        #print("ResNOT %s " % resNOT)
        return resNOT
    return noticias1

"""metodo para tokenizar consulta, especialmente palabras entre comillas"""
"""Ejemplo: hola OR "presidente de la neonazi" -> [[nombre_posting,hola], OR, ["headline",presidente de la neonazi]] """
"""Por edefecto, "text". Si no tiene, es una operacion"""
def token_consulta(consulta):
    terminos = []
    consultaToken = consulta.split()
    OPERACIONES = ['AND', 'OR', 'NOT', "ANDNOT", "NOTAND", "ORNOT", "NOTOR"]
    encontrar = None # encontrar nos sirve para ver si estamos dentro de una consultas entre comillas
    #Si es una consulta normal, sera none
    comilla = "\""
    notOperacion = False
    for t in consultaToken:

        if (t.find(comilla) == -1 and encontrar is None):

            if t not in OPERACIONES:
                if notOperacion == True:
                    terminos.append("AND")

                else:
                    notOperacion = True
            else:
                notOperacion = False
            aux = t.split(":")
            if len(aux) == 2:
                terminos.append(aux) # tiene un indice donde buscar indicado
            else:
                if t not in OPERACIONES: # Poner por defecto que busque en text
                    terminos.append(["text",t])
                else:
                    terminos.append(t) # si llegamos aqui, es una operacion
        else:
            #headline:"hola que"
            aux = t.split(":")
            if len(aux) != 2 and encontrar is None:
                indice = "text"
            elif encontrar is None:
                indice = aux[0]
                t = aux[1]

            if encontrar is None:
                encontrar = t[1:] # Le quitamos las primeras comillas
            else:
                if t[-1] == comilla:
                    encontrar = encontrar + " " + t[:-1] # le quitamos las ultmas comillas
                    terminos.append([indice,encontrar])
                    encontrar = None
                else: encontrar = encontrar + " " + t #o le quitamos nada

    return terminos

def buscar_y_pesar(palabra, posting, id_noticia):
    if len(posting) == 0:
        print("adios")
        return 0
    apariciones_en_doc = 0
    apariciones_totales = 0

    for i in range(len(posting)):
        x = posting[i]
        apariciones_totales = apariciones_totales + len(x[1])

        if x[0] == id_noticia:
            apariciones_en_doc = len(x[1])
            if apariciones_en_doc == 0:
                return 0

    peso = apariciones_en_doc / apariciones_totales
    return peso

#consulta es la lista de las palabras de la consulta concatenada, postings es una lista de las posting list de cada palabra,
def buscar_consecutivo_y_pesar(consulta, postings, id_noticia):
    list_posiciones = []

    # primero conseguimos las postings de la noticia a partir de las postings los terminos de la consulta
    for post in postings:
        for noticia in post:
            if (noticia[0] == id_noticia):
                list_posiciones.append(noticia[1])

    # una vez tenemos las postings de las noticias
    # vamos a ir recogiendo a pares las posibles ubicaciones de las palabras recorriendo cada posting y comparndola
    if(len(list_posiciones) == 0):
        return 0
    
    list_aux = list_posiciones[0]
    for i in range(0, len(list_posiciones) - 1):
        list1 = list_aux
        list2 = list_posiciones[i + 1]
        list_aux = []
        for x in list1:
            if x + 1 in list2:
                list_aux.append(x + 1)

    # en len(list_aux) tenemos el numero de apariciones de la consulta
    return len(list_aux)



def pasar_por_valor(funcion):
    def val(*args):
        cargs = [deepcopy(arg) for arg in args]
        return funcion(*cargs)
    return val

@pasar_por_valor
def por_valor(var):
    return var

def ordenar_noticias(lista_noticias_ref, consulta, postings):
    lista_noticias = por_valor(lista_noticias_ref)

    tipo_string = type('')
    lista_pesos_noticias = []

    #para cada noticia
    for noticia in lista_noticias:

        id_noticia = noticia[0]

        operador = "AND"
        termino = 0
        peso = 0
        peso_aux = 0

        #recorremos toda la consulta, diferenciando entre terminos y operadores
        for term in consulta:

            if type(term) == tipo_string:
                operador = term
            else:
                #esto es para texto mas adelante se podria implementar para ordenar por titulo (en caso de no querer ordenar por ellos, se haran excepciones)
                termino = 1
                lista_terminos = term[1].split(" ")

                #primero obtenemos las veces que aparece la palabra en la noticia
                if len(lista_terminos) == 1: #es una palabra
                    termino = 1
                    lista_terminos = term[1].split(" ")

                    if len(lista_terminos) == 1:
                        x = postings[term[0]].get(term[1], [])
                        peso_aux = buscar_y_pesar(term, x, id_noticia)

                else:
                    postings_conc = []
                    for x in lista_terminos:
                        postings_conc.append(postings[term[0]].get(x, []))

                    peso_aux = buscar_consecutivo_y_pesar(term, postings_conc, id_noticia)

            #calculamos el peso del termino con la consulta
            if termino == 1:
                termino = 0
                #si es una AND, sumamos los pesos
                if operador == "AND" or operador == "OR":
                    peso = peso + peso_aux

            operador = "AND" #en caso de que la consulta sea de tipo termino termino sin operadores en medio,

        #añadimos el peso total de la noticia
        lista_pesos_noticias.append(peso)


    #ordenamos la lista_noticias, segun el peso de lista_pesos..
    for i in range(0,len(lista_noticias)-1):
        for j in range(0,len(lista_noticias)-1):
            if lista_pesos_noticias[j] < lista_pesos_noticias[j+1]:
                lista_pesos_noticias[j], lista_pesos_noticias[j+1] = lista_pesos_noticias[j+1], lista_pesos_noticias[j]
                lista_noticias[j], lista_noticias[j+1] = lista_noticias[j+1], lista_noticias[j]

    return lista_noticias


def main(indices="index/index"):
    postings,lista_dias_noticias = recuperar_postings(indices)

    #print(type(postings))

    while True:
        consulta = input("Consulta: ")
        if consulta == "": break;
        l_consulta = token_consulta(consulta)
        size = len(l_consulta)
        iteracion = 0
        lista_noticias = None
        operacion = None
        operacionAnterior = None
        OPERACIONES = ['AND', 'OR', 'NOT']
        noticias = None

        while iteracion < size:

            palabra = l_consulta[iteracion]
            if palabra in OPERACIONES:
                operacionAnterior = operacion
                operacion = palabra
            else:
                lista_noticias = busqueda_noticias(palabra[1], postings[palabra[0]])
            iteracion += 1
            if operacion == "NOT":
                if lista_noticias is not None:
                    lista_noticias = switch(operacion, lista_noticias, None, lista_dias_noticias)
                    operacion = operacionAnterior

            if noticias is not None:
                if lista_noticias is not None:
                    noticias = switch(operacion, noticias, lista_noticias, lista_dias_noticias)
                    operacion = None
                    lista_noticias = None
            else:
                noticias = lista_noticias
                lista_noticias = None


        #fin while de consulta

        #se ordenan en la propia lista de noticias antes de la impresion

        noticias = ordenar_noticias(noticias, l_consulta, postings)

        #conseguir los ficheros de las noticias
        l_ficheros = list()

        for notice in noticias:
            fichero = (notice[0])[0]
            if fichero not in l_ficheros:
                l_ficheros.append(fichero)

        l_ficheros = ', '.join(l_ficheros)


        print("Aparece en %s noticias" % len(noticias), " en los ficheros: ", l_ficheros)

        if _DEBUG: continue;

        if noticias is not None and len(noticias) != 0:


            #noticias = devolver_noticias(noticias)

            noticias = devolver_noticias(noticias)
            long = len(noticias)
            if long > 5:
                for n in noticias[:10]:
                    print("---------------------- Titulo -------------------------")
                    print(n.titulo)
                    print("----------------------  -------------------------")
            elif long < 3: 
                for n in noticias:
                    print("---------------------- Titulo -------------------------")
                    print(n.titulo)
                    print("---------------------- Texto -------------------------")
                    print(n.texto)
                    print("----------------------  -------------------------")
            else:
            
                for n in noticias:
                    
                    
                    print("---------------------- Titulo -------------------------")
                    print(n.titulo)
                    print("---------------------- Snippet texto -------------------------")
                    print(sacar_snippet(n, l_consulta))
                    print("----------------------  -------------------------")



    print("Fin.")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        syntax()
        exit()
    indices = sys.argv[1]
    print("Proyecto realizado por Jose R. Prieto y Didac Bosch. \n \n")
    print("Pruebe a buscar algunos terminos en el documento. \n"
          "Para finalizar el buscador no introduzca ningun termino y presione enter"
          "\nLas ampliaciones realizadas son:"
          "\nBuscar terminos consecutivos, poniendolos entre comillas \"term1 term2 term3 term\""
          "\nOperaciones AND, NOT y OR entre terminos"
          "\nBuscar entre diferentes indices [headline, text, category, date], poniendo dos puntos antes del termino. Funciona con terminos consecutivos",
          "\nRankeado de noticias. \n")
    main(indices)