import os,sys,pickle, time
from xml.dom.minidom import parseString
from nltk.tokenize import RegexpTokenizer
import nltk
_DEBUG = False


pattern = r'\w+'
tokenizerAlpha = RegexpTokenizer(pattern)

def syntax():
    print("\n%s [Coleccion de noticias] [nombre del indice]\n" % sys.argv[0])
    sys.exit()

"""Anyade la raiz <xml/> al principio y al final del fichero sgml."""
def validarXML(xml, file=None):
    if file:
        fileXML = open(xml, "r",encoding="utf-8")
        xml = fileXML.read()
    xml = "<xml>" + xml + "</xml>"
    return xml

"""Devuelve los terminos de la noticia"""
def procesar_noticia(noticia):

    #pasamos to do a minus
    noticia = noticia.lower()
    # Quitamos simbolos no alfanumeicos
    terminos = nltk.regexp_tokenize(noticia, pattern) #leer el pattern mas arriba
    #No se han eliminado repetidos.
    #los eliminamos si queremos ahora
    #terminos = list(set(terminos))
    return terminos

"""Entrada: 19940108
salida: [08 01 1994]"""
def procesar_fecha(fecha):
    return [fecha[0:4],fecha[4:6],fecha[6:8]]


"""Anyade a la posting list del termino correspondiente la noticia en la que aparece.
Cada termino tiene una lista de noticias donde aparece
Ej termino Cafe:
#id_noticia = tupla (fichero, noticiaNumeroX)
Cafe [[id_noticia, 1], [noticia_2, 5], [noticia_58, 15], [noticia_X, YvecesAparece]]
YvecesAparece = array de pos
** -> Se ha anadido la posicion


Primer argumento_ una posting de la noticia con el mismo formato que la posting original/general

"""
def add_posting_to_postings(posting_noticia,postings, id_noticia):
    terminos_noticias = list(posting_noticia.keys())
    for termino in terminos_noticias:
        posting_termino_noticia = posting_noticia[termino]
        # posting_termino_noticia =-> ejemplo con cafe:  [id_noticia, [5 7 8 15]]
        post = postings.get(termino, [])
        # Ahora anyadimos la id con la posting creada  con el metodo auxiliar
        post.append([id_noticia,posting_termino_noticia])
        postings[termino] = post



"""-> Ahora se guarda en una segunda LISTA con todos los nombres de los ficheros que contienen noticias
y por cada fichero, el numero de noticias que tiene, para poder autogenerarse en el searcher
ejemplo: ['mini_enero/19940105.sgml', 31] -> ('mini_enero/19940105.sgml',1), ... , ('mini_enero/19940105.sgml',31)

lista: [['mini_enero/19940101.sgml', 213], ['mini_enero/19940102.sgml', 251], ['mini_enero/19940103.sgml', 457], ['mini_enero/19940104.sgml', 496], ['mini_enero/19940105.sgml', 594], ['mini_enero/19940106.sgml', 462], ['mini_enero/19940107.sgml', 468], ['mini_enero/19940108.sgml', 310], ['mini_enero/19940109.sgml', 303]]

"""
def add_noticias_to_list(num_noticias,dia, hash_noticias = []):

    hash_noticias.append([dia, num_noticias])
""""Posting temporal para la noticia, metodo auxiliar, SOLO estan las posiciones
cafe = [5 78 100 150]"""
def add_posting_noticia(termino,posicion, postings_noticias):
    post = postings_noticias.get(termino, [])
    post.append(posicion)
    postings_noticias[termino] = post



"""En disco se guardan dos hash
Las postings, con los terminos como Key
La hash de noticias (fichero, numNoticia) como Key, numero de veces que aparece como Valor

Se van a guardar ambos juntos como una dupla"""
def guardar_en_disco(indice,postings_text,postings_category,postings_headline,postings_date,hash_noticias):
    guardar = (postings_text,postings_category,postings_headline,postings_date, hash_noticias)
    filename = indice
    fWrite = open(filename,'wb')
    pickle.dump(guardar,fWrite,protocol=pickle.HIGHEST_PROTOCOL)
    #fWrite.write(obj)
    #fWrite.close()


"""Devuelve primero las postings, segundo la hash de noticias"""
def recuperar_postings(path):
    file = open(path, 'rb')
    recuperar = pickle.load(file)
    postings = dict()
    postings["text"] = recuperar[0]
    postings["category"] = recuperar[1]
    postings["headline"] = recuperar[2]
    postings["date"] = recuperar[3]
    return postings, recuperar[4]


def crear_carpetas(path):

    carpetas = path.split("/")[:-1]
    if len(carpetas) == 0: return;
    ruta = '/'.join(carpetas)
    respuestas = ["si", "s", "y", "of course", "por supuesto", "esta clarisimo que si", "so"]
    if not os.path.exists(ruta):
        pregunta = "La carpeta '" + ruta + "' en la que desea guardar el indice no existe. \n Desea crearla para poder continuar? [s/n]"
        ruta = input(pregunta)
        if ruta.lower() in respuestas:
            ruta = ""
            for carpeta in carpetas:
                if not os.path.exists(carpeta):
                    os.mkdir(carpeta)
        else:
            print("Ya que no podemos crear el indice debido a que no podemos crear la carpeta \n"
                  "Nos despedimos comunicandole que no podremos seguir con la ejecucion.\n"
                  "Por favor, vuelva a ejecutar el indexer con una ruta valida")
            exit()


def main(noticias,indice="index"):
    #postings donde se guardara to do
    #Ver metodo add_to_postings para ver una descripcion del contenido
    postings_text = dict()
    postings_category = dict()
    postings_headline = dict()
    postings_date = dict()
    """noticias = ('mini_enero/19940104.sgml', 68)"""
    hash_noticias = []
    if os.path.exists(indice):
        postings_text,hash_noticias = recuperar_postings(indice)
        print("Indice recuperado ")
        input("press para mostrar indice en text")
        print(postings_text["text"])
        input("press para mostrar indice en headline")
        print(postings_text["headline"])
        input("press para mostrar indice en date")
        print(postings_text["date"])
        input("press para mostrar las categorias")
        print(list(postings_text["category"].keys()))
        input("press para continuar y mostrar todas las noticias")
        print(hash_noticias)
        print("Tamano de %d dias" % len(hash_noticias))
        input("Press para mostrar el vocabulario en text ")
        print(" %s " % list(postings_text["text"].keys()))
        print(" tamano %d " % len(list(postings_text["text"].keys())))
        input("Press para continuar y salir del indexado (al encontrar un index, se da por indexado)")
        exit()
    dir = noticias
    crear_carpetas(indice)
    start = time.time()
    for (dirpath, dirnames, filenames) in os.walk(dir):
        filenames.sort()
        for filename in filenames:
            fichero_de_noticias = dirpath + "/" + filename
            dom = parseString(validarXML(fichero_de_noticias, file=True))
            # En dom tenemos to do ordenado ya
            docs = dom.getElementsByTagName('DOC')
            i = 0  # campo para ids, en cada documento empezara por 0

            for d in docs:
                # Creamos una posting temporal para la noticia en si
                posting_noticia = dict()
                posting_titulo = dict()
                posting_fecha = dict()
                posting_category = dict()

                fecha = d.getElementsByTagName('DATE')[0].firstChild.nodeValue
                titulo = d.getElementsByTagName('TITLE')[0].firstChild.nodeValue
                # Hay noticias sin categorizar... how to kill +1
                if d.getElementsByTagName('CATEGORY')[0].hasChildNodes():
                    category = d.getElementsByTagName('CATEGORY')[0].firstChild.nodeValue
                else:
                    category = ""
                texto = d.getElementsByTagName('TEXT')[0].firstChild.nodeValue
                #id = tupla (fichero, noticiaNumeroX)
                #carpeta -> dirpath.split("/")[1]
                id = (dir + "/" + filename,i)
                terminos_texto = procesar_noticia(texto)
                titulo_texto = procesar_noticia(titulo)
                category_texto = procesar_noticia(category)
                #fecha_texto = procesar_fecha(fecha)
                fecha_texto = [fecha]
                i += 1
                pos_termino = 0
                #por cada posting
                for termino in terminos_texto:
                    add_posting_noticia(termino,pos_termino,posting_noticia)

                    pos_termino += 1

                pos_termino = 0
                for termino in titulo_texto:
                    add_posting_noticia(termino, pos_termino, posting_titulo)

                    pos_termino += 1

                pos_termino = 0
                for termino in category_texto:
                    add_posting_noticia(termino, pos_termino, posting_category)

                    pos_termino += 1

                pos_termino = 0
                for termino in fecha_texto:
                    add_posting_noticia(termino, pos_termino, posting_fecha)

                    pos_termino += 1

                # To do lo de la noticia, se anyade a la posting general
                # Por cada posting
                add_posting_to_postings(posting_noticia, postings_text, id) # i = num_noticias
                add_posting_to_postings(posting_category, postings_category, id) # i = num_noticias
                add_posting_to_postings(posting_fecha, postings_date, id) # i = num_noticias
                add_posting_to_postings(posting_titulo, postings_headline, id) # i = num_noticias
            add_noticias_to_list(i,dir + "/" + filename, hash_noticias)
            #input("Press: ")
    end = time.time()
    print("He tardado %s segundos" % (end - start))
    #shutil.rmtree(temp, ignore_errors=True)  # borrar tmp
    guardar_en_disco(indice,postings_text,postings_category,postings_headline,postings_date,hash_noticias)

if __name__ == "__main__":
    if not _DEBUG:
        if len(sys.argv) < 2:
            syntax()
            exit()
        elif len(sys.argv) == 3:
            indice = sys.argv[2]
        else:
            indice = "index"
    noticias = sys.argv[1]

    main(noticias,indice=indice)


